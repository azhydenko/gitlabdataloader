//
//  Constants.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 1/30/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import Foundation

struct Constants {
    static let chosenCommitNotification = "chosenCommitNotification"
    static let commitsDownloadedNotification = "commitsDownloadedNotification"
    static let commitIdTag = "commitId"
    static let commitsDownloadedTag = "commitsDownloadedTag"
    
    static let locationsList = ["Lviv1" : (49.811715, 23.988997), "Lviv2" : (49.832617, 23.999398), "Lviv3" : (49.832117, 23.998668), "Lviv4" : (49.832791, 23.997382), "Lviv5" : (49.831695, 23.996578), "Lviv6" : (49.822065, 24.016458), "Lviv7" : (49.811162, 23.990250), "Lviv8" : (49.802782, 24.000805)]
    
    static let loadHistoryKey = "loadHistoryKey"
    static let ignoreMergesKey = "ignoreMergeKey"
    static let cacheDataKey = "cacheDataKey"
    static let showCoordinatesKey = "showCoordinatesKey"
    
    static let disclosureArrowIcon = "disclosureArrowIcon"
    
    static let mapsToUseKey = "mapsToUseKey"
    static let appleMaps = "Apple Maps"
    static let googleMaps = "Google Maps"
    static let yandexMaps = "Yandex Maps"
    
    static let gitHubClient = "gitHubClient"
    static let gitLabClient = "gitLabClient"
    static let bitBucketClient = "bitBucketClient"

}
