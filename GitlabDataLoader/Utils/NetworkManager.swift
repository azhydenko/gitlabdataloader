//
//  NetworkManager.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 1/31/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import Foundation

protocol CommitsDownloaderDelegate {
    func commitsDownloaded(resultSet: [CommitDataModel])
    func errorHandled(error: NSError)
}

class NetworkManager {
    
    public static func downloadCommitsInfo(url: URL, token: String, header: String, downloaderDelegate: CommitsDownloaderDelegate) {
        var commits = [CommitDataModel]()
        var request = URLRequest(url: url)

        request.setValue(token, forHTTPHeaderField: header)
        URLSession.shared.dataTask(with: request) { data, response, error in
            if error == nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [[String: Any]]
                    for jsonItem in json {
                        var dataModel = CommitDataModel()
                        dataModel.id = (jsonItem["id"] as? String)!
                        dataModel.shortId = (jsonItem["short_id"] as? String)!
                        dataModel.title = (jsonItem["title"] as? String)!
                        dataModel.createdAt = (jsonItem["created_at"] as? String)!
                        dataModel.message = (jsonItem["message"] as? String)!
                        dataModel.authorName = (jsonItem["author_name"] as? String)!
                        dataModel.authorEmail = (jsonItem["author_email"] as? String)!
                        dataModel.authoredDate = (jsonItem["authored_date"] as? String)!
                        dataModel.committerName = (jsonItem["committer_name"] as? String)!
                        dataModel.committerEmail = (jsonItem["committer_email"] as? String)!
                        dataModel.committedDate = (jsonItem["committed_date"] as? String)!
                        commits.append(dataModel)
                    }
                    downloaderDelegate.commitsDownloaded(resultSet: commits)
                } catch let error as NSError {
                    downloaderDelegate.errorHandled(error: error)
                }
            } else {
                downloaderDelegate.errorHandled(error: error! as NSError)
            }
        }.resume()
    }
    
}
