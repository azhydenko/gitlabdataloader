//
//  CommitDataModel.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 1/25/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

struct CommitDataModel {
    var title: String = ""
    var subTitle: String = ""
    var id: String = ""
    var shortId: String = ""
    var createdAt: String = ""
    var message: String = ""
    var authorName: String = ""
    var authorEmail: String = ""
    var authoredDate: String = ""
    var committerName: String = ""
    var committerEmail: String = ""
    var committedDate: String = ""
}
