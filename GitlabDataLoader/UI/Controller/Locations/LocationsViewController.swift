//
//  LocationsViewController.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 2/6/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import UIKit

class LocationsViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private let locationsSegmentTitle = "Locations"
    private let mapSegmentTitle = "Map"
    
    private let locationTableViewIdentifier = "LocationsTableViewController"
    private let locationMapTableViewIdentifier = "LocationsMapViewController"
    
    private let mainStoryBoardTitle = "Main"
    
    private lazy var locationsViewController: LocationsTableViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: mainStoryBoardTitle, bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: locationTableViewIdentifier) as! LocationsTableViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var mapViewController: LocationsMapViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: mainStoryBoardTitle, bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: locationMapTableViewIdentifier) as! LocationsMapViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }
    
    @IBAction func segmentSelectionChanged(_ sender: UISegmentedControl) {
        updateView()
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    private func updateView() {
        if segmentedControl.selectedSegmentIndex == 0 {
            remove(asChildViewController: mapViewController)
            add(asChildViewController: locationsViewController)
        } else {
            remove(asChildViewController: locationsViewController)
            add(asChildViewController: mapViewController)
        }
    }

    private func setupView() {
        setupSegmentedControl()
        
        updateView()
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    private func setupSegmentedControl() {
        // Configure Segmented Control
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: locationsSegmentTitle, at: 0, animated: true)
        segmentedControl.insertSegment(withTitle: mapSegmentTitle, at: 1, animated: true)
        
        // Select First Segment
        segmentedControl.selectedSegmentIndex = 0
    }

}
