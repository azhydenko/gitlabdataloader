//
//  LocationsTableTableViewController.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 2/6/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import UIKit

class LocationsTableViewController: UITableViewController {

    @IBOutlet weak var locationsTableView: UITableView!
    
    private let locationCellIdentifier = "locationCellIdentifier"
    private var locationNames = [String]()
    private var locationCoordinates = [(Double, Double)]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationNames = Array(Constants.locationsList.keys)
        locationCoordinates = Array(Constants.locationsList.values)
        
        locationsTableView.register(CustomCell.self, forCellReuseIdentifier: locationCellIdentifier)
        locationsTableView.tableFooterView = UIView()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = locationsTableView.dequeueReusableCell(withIdentifier: locationCellIdentifier) as! CustomCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.gray
        let rowIndex = indexPath.row
        cell.title = locationNames[rowIndex]
        cell.subTitle = String(locationCoordinates[rowIndex].0) + String(locationCoordinates[rowIndex].1)
        cell.layoutSubviews()
        return cell
    }
    
}
