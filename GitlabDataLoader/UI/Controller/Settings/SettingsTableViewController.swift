//
//  SettingsTableViewController.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 2/6/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var loadHistorySwitch: UISwitch!
    @IBOutlet weak var ignoreMergesSwitch: UISwitch!
    @IBOutlet weak var cacheDataSwitch: UISwitch!
    
    @IBOutlet weak var gitHubSwitch: UISwitch!
    @IBOutlet weak var gitLabSwitch: UISwitch!
    @IBOutlet weak var bitBucketSwitch: UISwitch!
    
    @IBOutlet weak var showCoordinatesSwitch: UISwitch!
    @IBOutlet weak var mapsToUseLabel: UILabel!
    
    @IBOutlet weak var mapsToUseCell: UITableViewCell!
    
    private let defaults = UserDefaults.standard
    
    private var rowHeight: CGFloat = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSettings()
    }
    
    override func viewDidLayoutSubviews() {
        view.layoutIfNeeded()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mapToUseTapped(indexPath)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        let isOn = sender.isOn
        switch sender {
        case loadHistorySwitch:
            defaults.set(isOn, forKey: Constants.loadHistoryKey)
        case ignoreMergesSwitch:
             defaults.set(isOn, forKey: Constants.ignoreMergesKey)
        case cacheDataSwitch:
             defaults.set(isOn, forKey: Constants.cacheDataKey)
        case showCoordinatesSwitch:
            defaults.set(isOn, forKey: Constants.showCoordinatesKey)
        case gitHubSwitch:
            defaults.set(isOn, forKey: Constants.gitHubClient)
        case gitLabSwitch:
            defaults.set(isOn, forKey: Constants.gitLabClient)
        case bitBucketSwitch:
            defaults.set(isOn, forKey: Constants.bitBucketClient)
        default:
            break
        }
    }
    
    func mapToUseTapped(_ indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) == mapsToUseCell {
            let alert = UIAlertController(title: NSLocalizedString("mapsToUseInApp", comment: "Maps to use in app string"),
                                          message: NSLocalizedString("mapsSelection", comment: "Maps to chose string"),
                                          preferredStyle: .alert)
            let appleMapAction = UIAlertAction(title: Constants.appleMaps, style: .default, handler: { (action) -> Void in
                self.defaults.set(Constants.appleMaps, forKey: Constants.mapsToUseKey)
                self.initMapsToUse()
            })
            
            let googleMapAction = UIAlertAction(title: Constants.googleMaps, style: .default, handler: { (action) -> Void in
                self.defaults.set(Constants.googleMaps, forKey: Constants.mapsToUseKey)
                self.initMapsToUse()
            })
            
            let yandexMapAction = UIAlertAction(title: Constants.yandexMaps, style: .default, handler: { (action) -> Void in
                self.defaults.set(Constants.yandexMaps, forKey: Constants.mapsToUseKey)
                self.initMapsToUse()
            })
            
            // Cancel button
            let cancel = UIAlertAction(title: NSLocalizedString("cancel", comment: "Cancel string"), style: .destructive, handler: { (action) -> Void in })
            alert.addAction(appleMapAction)
            alert.addAction(googleMapAction)
            alert.addAction(yandexMapAction)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func initSettings() {
        // commits settings
        loadHistorySwitch.setOn(defaults.bool(forKey: Constants.loadHistoryKey), animated: false)
        ignoreMergesSwitch.setOn(defaults.bool(forKey: Constants.ignoreMergesKey), animated: false)
        cacheDataSwitch.setOn(defaults.bool(forKey: Constants.cacheDataKey), animated: false)
        showCoordinatesSwitch.setOn(defaults.bool(forKey: Constants.showCoordinatesKey), animated: false)
    
        // git clients settings
        gitHubSwitch.setOn(defaults.bool(forKey: Constants.gitHubClient), animated: false)
        gitLabSwitch.setOn(defaults.bool(forKey: Constants.gitLabClient), animated: false)
        bitBucketSwitch.setOn(defaults.bool(forKey: Constants.bitBucketClient), animated: false)
        
        initMapsToUse()
    }
    
    func initMapsToUse() {
        let mapValue = UserDefaults.standard.string(forKey: Constants.mapsToUseKey)
        
        mapsToUseLabel.text = NSLocalizedString("mapsToUse", comment: "Maps to use string")
        if mapValue != nil {
            mapsToUseLabel.text?.append(" (\(mapValue!))")
        }
    }

}
