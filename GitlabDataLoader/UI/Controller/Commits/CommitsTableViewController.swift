//
//  ViewController.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 1/25/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import UIKit

class CommitsTableViewController : UIViewController, UITableViewDataSource, UITableViewDelegate, CommitsDownloaderDelegate {
    
    @IBOutlet weak var commitsTable: UITableView!
    private var noDataLabel = UILabel()
    
    private let gitlabAPIUrl = "https://gitlab.com/api/v4/projects/2959204/repository/commits"
    private let privateTokenValue = "_jzgm7f_-A5CjhrN2Es-"
    private let headerPrivateToken = "PRIVATE-TOKEN"
    private let commitCellIdentifier = "commitCellIdentifier"
    private let detailViewController = "DetailViewController"
    private let deinitMessage = "CommitsTableViewController deinit"
    private let rowHeight: CGFloat = 65
    
    
    private var tableData = [CommitDataModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        commitsTable.register(CustomCell.self, forCellReuseIdentifier: commitCellIdentifier)
        commitsTable.tableFooterView = UIView()
        commitsTable.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: { () -> Void in
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: Constants.chosenCommitNotification), object: nil, userInfo: [Constants.commitIdTag: self.tableData[indexPath.row]]))
        })
        
        let controller = storyboard?.instantiateViewController(withIdentifier: detailViewController)
        self.navigationController!.pushViewController(controller!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = commitsTable.dequeueReusableCell(withIdentifier: commitCellIdentifier) as! CustomCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.gray
        cell.title = tableData[indexPath.row].title
        cell.subTitle = tableData[indexPath.row].subTitle
        cell.layoutSubviews()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowsCount = tableData.count
        showNoDataMock(numOfRows: rowsCount)
        return rowsCount
    }
    
    func showNoDataMock(numOfRows: Int) {
        if noDataLabel.text == nil {
            noDataLabel.frame = self.view.bounds
            noDataLabel.text = NSLocalizedString("noDataAvailable", comment: "No data available string")
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            self.view.addSubview(noDataLabel)
        }
        if numOfRows == 0 {
            commitsTable.isHidden = true
            self.noDataLabel.isHidden = false
        } else {
            commitsTable.isHidden = false
            self.noDataLabel.isHidden = true
        }
        
    }
    
    @IBAction func loadGitData(_ sender: Any) {
        NetworkManager.downloadCommitsInfo(url: URL(string: gitlabAPIUrl)!, token: privateTokenValue, header: headerPrivateToken, downloaderDelegate: self)
    }
    
    func showErrorAlert(_ errorWhileLoading: NSError) {
        let alert = UIAlertController(title: NSLocalizedString("errorOccurred", comment: "Message that appears in case of downloading error"), message: errorWhileLoading.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: "OK label"), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    } 
    
    func commitsDownloaded(resultSet: [CommitDataModel]) {
        tableData = resultSet
        DispatchQueue.main.async(execute: { () -> Void in
            self.commitsTable.reloadData()
        })
    }
    
    func errorHandled(error: NSError) {
        showErrorAlert(error)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print(deinitMessage)
    }
    
}
