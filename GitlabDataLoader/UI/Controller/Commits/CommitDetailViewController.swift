//
//  DetailViewController.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 1/26/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import UIKit

class CommitDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var shortIdLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var authorEmailLabel: UILabel!
    @IBOutlet weak var committerNameLabel: UILabel!
    @IBOutlet weak var committerEmailLabel: UILabel!
    @IBOutlet weak var committedDateLabel: UILabel!
    @IBOutlet weak var authoredDateLabel: UILabel!
    
    private let deinitMessage = "DetailViewController deinit"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(someCommitTapped(notification:)), name: NSNotification.Name(rawValue: Constants.chosenCommitNotification), object: nil)
    }
    
    @objc func someCommitTapped(notification:Notification)
    { 
        let commitData = notification.userInfo?[Constants.commitIdTag] as! CommitDataModel
        updateCommitData(commitData)
    }
    
    func updateCommitData(_ commitData: CommitDataModel) {
        let subTitle = commitData.subTitle.isEmpty ? "-" : commitData.subTitle
        
        titleLabel.text = titleLabel.text! + commitData.title
        subTitleLabel.text = subTitleLabel.text! + subTitle
        idLabel.text = idLabel.text! + commitData.id
        shortIdLabel.text = shortIdLabel.text! + commitData.shortId
        createdAtLabel.text = createdAtLabel.text! + commitData.createdAt
        messageLabel.text = messageLabel.text! + commitData.message.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        authorNameLabel.text = authorNameLabel.text! + commitData.authorName
        authorEmailLabel.text = authorEmailLabel.text! + commitData.authorEmail
        committerNameLabel.text = committerNameLabel.text! + commitData.committerName
        committerEmailLabel.text = committerEmailLabel.text! + commitData.committerEmail
        committedDateLabel.text = committedDateLabel.text! + commitData.committedDate
        authoredDateLabel.text = authoredDateLabel.text! + commitData.authoredDate
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print(deinitMessage)
    }
    
}
