//
//  MainTabBarController.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 2/4/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    var bottomTabBarItem = UITabBarItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : self.view.tintColor], for: .selected)
    }

}
