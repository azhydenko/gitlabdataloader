//
//  CommitCell.swift
//  GitlabDataLoader
//
//  Created by Andrii Zhydenko on 1/26/19.
//  Copyright © 2019 Andrii Zhydenko. All rights reserved.
//

import Foundation
import UIKit

class CustomCell : UITableViewCell {
    let titleLabelSize = 18
    let subTitleLabelSize = 14
    let containerHorizontalMargin: CGFloat = 20
    let containerVerticalMargin: CGFloat = 10
    
    var title: String?
    var subTitle: String?
    
    var titleView: UILabel = {
        var titleView = UILabel()
        titleView.translatesAutoresizingMaskIntoConstraints = false
        return titleView
    }()
    
    var subTitleView: UILabel = {
        var subTitleView = UILabel()
        subTitleView.translatesAutoresizingMaskIntoConstraints = false
        return subTitleView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.addSubview(titleView)
        self.addSubview(subTitleView)

        initConstraints()
    }
    
    func initConstraints() {
        titleView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: containerHorizontalMargin).isActive = true
        titleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -containerHorizontalMargin).isActive = true
        titleView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        titleView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = false
        
        subTitleView.topAnchor.constraint(equalTo: self.titleView.bottomAnchor).isActive = true
        subTitleView.leftAnchor.constraint(equalTo: self.titleView.leftAnchor).isActive = true
        subTitleView.rightAnchor.constraint(equalTo: self.titleView.rightAnchor).isActive = true
        subTitleView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -containerVerticalMargin).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleView.font = titleView.font.withSize(CGFloat(titleLabelSize))
        subTitleView.font = subTitleView.font.withSize(CGFloat(subTitleLabelSize))

        if let title = title {
            titleView.text = title
        }
        if let subTitle = subTitle {
            subTitleView.text = subTitle.isEmpty ? NSLocalizedString("noSubtitle", comment: "Label when we do not have subtitle") : subTitle
        }
    }
    
}
